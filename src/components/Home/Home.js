import React, { Component } from 'react';
import Navbar from '../Navbar/Navbar';
import Table from '../Table/Table';

export default class Home extends Component {

    render() {
        return (
            <div>
                <Navbar />
                <br />
                <Table />
            </div>
        )
    }
}