import React, { Component } from 'react';
import ReactTable from 'react-table';
import axios from 'axios';
import Data from './Data';
// import {
//     refreshLocalStorage,
//     handleChangePage,
//     handleChangeRowsPerPage
// } from './PaginationHandler';
import './Table.css';


export default class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: Data[0].shipments,
            constData: Data[0].shipments,
            pageSize: 5,
            pageIndex: 0,
            searchInput: '',
        }
    }

    componentDidMount() {
        console.log("data", this.state.data)
        axios.get(`https://github.com/typicode/json-server/blob/master/db.json`,

            {
                headers: {
                    client_id: '3f146988ff07f9be1864',
                    client_secret: 'a91df0496f13df72a72e9e849954610851b16aa5'

                    // "Access-Control-Allow-Origin": "http://localhost:3000",
                    // Authorization: 'Token a91df0496f13df72a72e9e849954610851b16aa5'
                }
            })

            .then((response) => {
                console.log('response', response)
            })
            .catch((error) => {
                if (error && error.response && error.response.status === 401) {
                    console.log('Error')
                }
            });
    }

    onPageChange = (pageIndex) => {
        console.log('onPageChange', pageIndex)
        this.setState({ pageIndex });
    }

    onPageSizeChange = (pageSize, pageIndex) => {
        console.log('onPageSizeChange', pageSize, pageIndex)
        this.setState({ pageSize })
    }

    inputHandler = (event) => {
        this.setState({ searchInput: event.target.value }, () => {
            if (!this.state.searchInput) {
                this.setState({ data: this.state.constData });
            }
        });
    }

    searchHandler = () => {

        if (this.state.searchInput) {

            const indexOfLastPost = (this.state.pageIndex + 1) * this.state.pageSize;
            const indexOfFirstPost = indexOfLastPost - this.state.pageSize;
            const currentPosts = this.state.constData.slice(indexOfFirstPost, indexOfLastPost);

            console.log('newData', currentPosts)
            if (this.state.searchInput.length > 0) {
                const newData = currentPosts.filter(data => {
                    if (data.mode.toLowerCase().includes(this.state.searchInput.toLowerCase())
                        || data.name
                            .toLowerCase()
                            .includes(this.state.searchInput.toLowerCase())
                        || data.origin
                            .toLowerCase()
                            .includes(this.state.searchInput.toLowerCase())
                        || data.total
                            .includes(this.state.searchInput)
                    )
                        return data
                });
                console.log('search Result', newData)
                this.setState({ data: newData })
            }
        }
    }

    render() {

        return (
            <div>

                <input className="input-text" type="search" placeholder="Search..."
                    value={this.state.searchInput} onChange={(event) => this.inputHandler(event)}
                />
                <button className="search-btn"
                    type="button" onClick={() => this.searchHandler()}>Search</button>

                <ReactTable
                    data={this.state.data ? this.state.data : []}
                    columns={[
                        {
                            Header: () => <div className="ID">ID</div>,
                            accessor: 'id',
                            className: 'text-center',
                            sortable: false,
                            filterable: false,
                            foldable: true,
                            width: 75
                        },
                        {
                            Header: () => <div className="Header" >Name</div>,
                            accessor: 'name',
                            className: 'text-center',
                            foldable: true,
                            filterable: false,
                            width: 400

                        },
                        {
                            Header: () => <div className="Header" >Mode</div>,
                            accessor: 'mode',
                            foldable: true,
                            className: 'text-center',
                        },
                        {
                            Header: () => <div className="Header" >Origin</div>,
                            accessor: 'origin',
                            foldable: true,
                            className: 'text-center',
                        },
                        {
                            Header: () => <div className="Header" >Total</div>,
                            accessor: 'total',
                            foldable: true,
                            className: 'text-center',
                        },
                    ]}
                    pageSize={this.state.pageSize}
                    showPaginationBottom={true}
                    pageIndex={this.state.pageIndex}
                    onPageChange={(pageIndex) => this.onPageChange(pageIndex)}
                    onPageSizeChange={(pageSize, pageIndex) => { this.onPageSizeChange(pageSize, pageIndex) }}
                />

            </div>
        )
    }
}